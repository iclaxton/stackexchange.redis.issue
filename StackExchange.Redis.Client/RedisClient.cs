﻿using System;
using System.Threading.Tasks;

namespace StackExchange.Redis.Client
{
    public sealed class RedisClient : IDisposable
    {
        private ConnectionMultiplexer _server;

        public async Task ConnectAsync(string host, int port, string auth, bool useSsl = true)
        {
            var configurationOptions = new ConfigurationOptions
            {
                AbortOnConnectFail = false,
                ConnectTimeout = 5000
            };

            configurationOptions.EndPoints.Add(host, port);

            if (!string.IsNullOrWhiteSpace(auth))
                configurationOptions.Password = auth;

            if (useSsl)
                configurationOptions.Ssl = true;

            _server = await ConnectionMultiplexer.ConnectAsync(configurationOptions).ConfigureAwait(false);
        }

        public void Dispose()
        {
            _server?.Dispose();
        }
    }
}
