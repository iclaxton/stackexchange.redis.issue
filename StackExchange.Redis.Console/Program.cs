﻿using StackExchange.Redis.Client;

namespace StackExchange.Redis.Console
{
    internal static class Program
    {
        private static int Main(string[] args)
        {
            if (args.Length != 3)
                return -1;

            var host = args[0];
            var port = int.Parse(args[1]);
            var auth = args[2];

            var redisClient = new RedisClient();

            redisClient.ConnectAsync(host, port, auth).GetAwaiter().GetResult();

            return 0;
        }
    }
}